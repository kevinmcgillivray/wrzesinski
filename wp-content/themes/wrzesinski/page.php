<?php get_header(); ?>
	<div class="row">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	   <div class="col-md-8 col-md-offset-2">
	   <h1><?php the_title(); ?></h1>	
	   	<?php the_content(); ?>

		<?php endwhile; endif; ?>
		</div>
	</div>
<?php get_footer(); ?>