<?php get_header(); ?>

			<div id="carousel" class="carousel slide" data-ride="carousel" data-interval="false">
        	
				<ol class="carousel-indicators">
					<li data-target="#carousel" data-slide-to="0" class="active"></li>
					<li data-target="#carousel" data-slide-to="1"></li>
					<li data-target="#carousel" data-slide-to="2"></li>
				</ol>
				
				<div class="carousel-inner">
					<div class="item active">
						<iframe width="988" height="556" src="//www.youtube.com/embed/nsfh9dj5HxI?rel=0" frameborder="0" allowfullscreen></iframe>
					</div>
					<div class="item">
						<iframe width="988" height="556" src="//www.youtube.com/embed/1nfGg7O70Gw?rel=0" frameborder="0" allowfullscreen></iframe>
					</div>
					<div class="item">
						<iframe width="988" height="556" src="//www.youtube.com/embed/nsfh9dj5HxI?rel=0" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
				
				<a class="left carousel-control" href="#carousel" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left"></span>
				</a>
				<a class="right carousel-control" href="#carousel" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right"></span>
				</a>
			</div>
			
			<div id="bio-banner">
				<p id="short-bio"><?php bloginfo( 'description' ); ?></p>
			</div>
			
			<div class="row">
				<div class="col-xs-6">
					<h3>Upcoming Shows</h3>
				</div>
				<div class="col-xs-6">
					<a class="btn btn-lg btn-primary pull-right">See all</a>
				</div>
			</div>
			<hr>
			<?php
				$options = array('scope' => 'upcoming', 'group_artists' => 'no');
				echo gigpress_shows($options);
			?>

<?php get_footer(); ?>