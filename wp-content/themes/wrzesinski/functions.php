<?php

// Load jQuery
if ( !is_admin() ) {
   wp_deregister_script('jquery');
   wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"), false);
   wp_enqueue_script('jquery');
}

// Bootstrap Javascript
function bootstrap_scripts_with_jquery() {
    wp_register_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ) );
    wp_enqueue_script( 'bootstrap' );
}
add_action( 'wp_enqueue_scripts', 'bootstrap_scripts_with_jquery' );

function iframes() {
    wp_register_script( 'iframes', get_template_directory_uri() . '/js/jquery.fitvids.js', array( 'jquery' ) );
    wp_enqueue_script( 'iframes' );
}
add_action( 'wp_enqueue_scripts', 'iframes' );

// Register custom navigation walker
require_once('wp_bootstrap_navwalker.php');

function register_my_menu() {
	register_nav_menu('header-menu',__( 'Header Menu' ));
}
add_action( 'init', 'register_my_menu' );

// Replaces the excerpt "more" text by a link
function new_excerpt_more($more) {
       global $post;
	return '<a class="moretag" href="'. get_permalink($post->ID) . '"> Read more...</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');

?>