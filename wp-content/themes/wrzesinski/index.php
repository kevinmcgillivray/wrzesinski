<?php get_header(); ?>

<div class="row">
	<div class="col-md-9">
		<h1>Stories from the Road</h1>
		<?php query_posts('cat=-1'); ?>
		<?php while (have_posts()) : the_post(); ?>
			<hr>
			<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<p><em><?php the_time('l, F jS, Y'); ?> | Posted by <?php the_author(); ?></em></p>
			<?php the_excerpt(); ?>
		<?php endwhile; ?>
	</div>
	<div class="col-md-3">
		<?php get_sidebar(); ?>
	</div>
</div>

<?php get_footer(); ?>