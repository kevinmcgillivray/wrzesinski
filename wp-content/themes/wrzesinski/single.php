<?php get_header(); ?>

<div class="row">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<div class="col-md-9">
	<h1><?php the_title(); ?></h1>
	<p><em><?php the_time('l, F jS, Y'); ?> | Posted by <?php the_author(); ?></em></p>
	<?php the_content(); ?>
	</div>
	<?php endwhile; endif; ?>
	<div class="col-md-3">
		<?php get_sidebar(); ?>
	</div>
</div>

<?php get_footer(); ?>