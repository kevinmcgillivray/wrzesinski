<?php get_header(); ?>

	<h2>Oops, I can't find the page you are looking for.</h2>
	<p>Can you try again or maybe visit the <a href="<?php echo get_option('home'); ?>">home page</a> to start fresh?</p>

<?php get_footer(); ?>