<?php
/*
Template Name: Shows
*/
?>

<?php get_header(); ?>
	<div class="row">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	   <div class="col-md-12">
	   <h2><?php the_title(); ?></h2>	
	   	<?php the_content(); ?>
		<?php
		    $options = array('scope' => 'upcoming', 'group_artists' => 'no');
		    echo gigpress_shows($options);
		?>

		<?php endwhile; endif; ?>
		</div>
	</div>
<?php get_footer(); ?>