<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.png" />
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
	<?php wp_head(); ?>
	<script type="text/javascript" src="//use.typekit.net/ytn4vdb.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
	<script>
	  $(document).ready(function(){
	    // Target your .container, .wrapper, .post, etc.
	    $(".item").fitVids();
	  });
	</script>
</head>

<body>
	<header>
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<h1 id="name"><a href="<?php echo get_option('home'); ?>/">Justin Wrzesinski</a></h1>
				</div>
				<div class="col-md-6">
					<ul class="pull-right" id="social-media">
						<li><a href="http://www.facebook.com/justinwmusic" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/facebook.png"></a></li>
						<li><a href="http://www.youtube.com/justinwrzesinski" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/youtube.png"></a></li>
						<li><a href="http://www.myspace.com/justinwrzesinski" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/myspace.png"></a></li>
						<li><a href="http://www.justinwmusic.com/contact"><img src="<?php bloginfo('template_url'); ?>/images/newsletter.png"></a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="navbar-wrapper">
				<div class="navbar navbar-default" role="navigation">
					<div class="container">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
							    <span class="icon-bar"></span>
							    <span class="icon-bar"></span>
							    <span class="icon-bar"></span>
							 </button>
						</div>
			   			<?php wp_nav_menu( array( 
			   				'theme_location' => 'header-menu',
			   				'depth'			 => 1,
			   				'container'		 => 'div',
			   				'container_class'   => 'collapse navbar-collapse',
			   				'menu_class'        => 'nav navbar-nav',
			   				'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
			   				'walker'            => new wp_bootstrap_navwalker()) 
			   				); 
			   			?>
					</div>
				</div>
		</div>
	</header>
	<div class="container">
		<div class="main-content">